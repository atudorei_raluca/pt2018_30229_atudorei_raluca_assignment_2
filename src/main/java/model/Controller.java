package model;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

public class Controller {
	private View view;
	private Model model;
	public Controller(View view, Model model) {
		this.view= view;
		this.model= model;
		view.addStartListener(new StartListener());
	}
	
	class StartListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String minIntArriving= view.getMinIntArriving();
			model.setMinIntArriving(minIntArriving);
			String maxIntArriving= view.getMaxIntArriving();
			model.setMaxIntArriving(maxIntArriving);
			String minServiceTime= view.getMinServiceTime();
			model.setMinServiceTime(minServiceTime);
			String maxServiceTime= view.getMaxServiceTime();
			model.setMaxServiceTime(maxServiceTime);
			String nrQueues= view.getNrQueues();
			model.setNrQueues(nrQueues);
			String simInt= view.getSimInt();
			model.setSimInt(simInt);
			String nrClients= view.getNrClients();
			model.setNrClients(nrClients);
			String result= model.getResult();
			view.setResult(result);
		}
	}
}
