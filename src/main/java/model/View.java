package model;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class View extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField minIntArriving = new JTextField();
	private JTextField maxIntArriving = new JTextField();
	private JTextField minServiceTime = new JTextField();
	private JTextField maxServiceTime = new JTextField();
	private JTextField nrQueues = new JTextField();
	private JTextField nrClients = new JTextField();
	private JTextField simInt = new JTextField();
	private JTextField output = new JTextField();
	private JButton start = new JButton("Start");

	public View() {
		initUI();
	}

	public void initUI() {

		JFrame frame = new JFrame("Tema 2");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(700, 500);
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		JPanel panel3 = new JPanel();
		JPanel panel4 = new JPanel();
		JPanel panel5 = new JPanel();

		minIntArriving.setPreferredSize(new Dimension(50, 24));
		maxIntArriving.setPreferredSize(new Dimension(50, 24));
		minServiceTime.setPreferredSize(new Dimension(50, 24));
		maxServiceTime.setPreferredSize(new Dimension(50, 24));
		nrQueues.setPreferredSize(new Dimension(50, 24));
		nrClients.setPreferredSize(new Dimension(50, 24));
		simInt.setPreferredSize(new Dimension(50, 24));
		output.setPreferredSize(new Dimension(100, 50));

		panel1.add(new JLabel("Interval between customers: "));
		panel1.add(minIntArriving);
		panel1.add(maxIntArriving);
		panel1.setLayout(new FlowLayout());

		panel2.add(new JLabel("Service time: "));
		panel2.add(minServiceTime);
		panel2.add(maxServiceTime);
		panel2.setLayout(new FlowLayout());

		panel3.add(new JLabel("Number of queues: "));
		panel3.add(nrQueues);

		panel4.add(new JLabel("Simulation interval: "));
		panel4.add(simInt);
		
		panel5.add(new JLabel("Number of clients: "));
		panel5.add(nrClients);

		JPanel p = new JPanel();
		p.add(panel1);
		p.add(panel2);
		p.add(panel3);
		p.add(panel4);
		p.add(panel5);
		p.add(start);
		p.add(output);

		p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
		frame.setContentPane(p);
		frame.setVisible(true);

		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public void addStartListener(ActionListener s) {
		start.addActionListener(s);
	}
	
	public String getMinIntArriving() {
		return minIntArriving.getText();
	}

	public String getMaxIntArriving() {
		return maxIntArriving.getText();
	}
	
	public String getMinServiceTime() {
		return minServiceTime.getText();
	}
	
	public String getMaxServiceTime() {
		return maxServiceTime.getText();
	}
	
	public String getNrQueues() {
		return nrQueues.getText();
	}
	
	public String getSimInt() {
		return simInt.getText();
	}
	
	public void setResult(String o) {
		output.setText(o);
	}

	public String getNrClients() {
		return nrClients.getText();
	}

	public void setNrClients(JTextField nrClients) {
		this.nrClients = nrClients;
	}

}
