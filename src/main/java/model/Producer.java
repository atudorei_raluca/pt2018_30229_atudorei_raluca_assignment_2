package model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import util.ClientLogger;
import util.TimeUtil;

public class Producer implements Runnable {

	private List<QueueClient> cozi;
	private ClientLogger logger;
	private int currentIdClient = 1;
	private int nrCozi, simInt;
	private int minTimeBetween, maxTimeBetween, minServiceTime, maxServiceTime;

	public Producer(int nrCozi, int simInt, int minTimeBetween, int maxTimeBetween, int minServiceTime,
			int maxServiceTime, ClientLogger logger) {
		this.minTimeBetween = minTimeBetween;
		this.maxTimeBetween = maxTimeBetween;
		this.minServiceTime = minServiceTime;
		this.maxServiceTime = maxServiceTime;
		this.logger = logger;
		this.nrCozi = nrCozi;
		this.simInt = simInt;
		cozi = new ArrayList<QueueClient>();
		for (int i = 0; i < nrCozi; i++) {
			QueueClient coadaClienti = new QueueClient(i + 1, logger);
			cozi.add(coadaClienti);
			coadaClienti.start();
		}
	}

	public Client randomClientGenerator() {
		int serviceTime = minServiceTime + (int) (Math.random() * ((maxServiceTime - minServiceTime) + 1));
		Client c = new Client(currentIdClient, serviceTime);
		currentIdClient++;
		return c;

	}

	public int getCoadaMinClienti() {
		int index = 0;
		long min = cozi.get(0).size();
		for (int i = 1; i < nrCozi; i++) {
			long lungimeCoadaCurenta = cozi.get(i).size();
			if (min > lungimeCoadaCurenta) {
				min = lungimeCoadaCurenta;
				index = i;
			}
		}
		return index;
	}

	private void adaugaClientInCoada(Client c, Calendar crtTime) {
		cozi.get(getCoadaMinClienti()).addClient(c);
		logger.logAddClient(c, crtTime);
	}

	public void run() {
		TimeUtil.simulationTime = Calendar.getInstance();
		TimeUtil.simulationTime.add(Calendar.SECOND, simInt);
		Calendar crtTime = Calendar.getInstance();
		try {
			while (crtTime.compareTo(TimeUtil.simulationTime) <= 0) {
				Client c = randomClientGenerator();
				adaugaClientInCoada(c, crtTime);
				int timeBetween = minTimeBetween + (int) (Math.random() * ((maxTimeBetween - minTimeBetween) + 1));
				Thread.sleep(timeBetween * 1000);
				crtTime = Calendar.getInstance();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for(QueueClient qc: cozi) {
			qc.setSfarsitOrar(true);
		}
		for(QueueClient qc: cozi) {
			try {
				qc.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Am terminat simularea la: "+TimeUtil.getFancyTime(Calendar.getInstance()));
	}

	public int getMinServiceTime() {
		return minServiceTime;
	}

	public void setMinServiceTime(int minServiceTime) {
		this.minServiceTime = minServiceTime;
	}

	public int getMaxServiceTime() {
		return maxServiceTime;
	}

	public void setMaxServiceTime(int maxServiceTime) {
		this.maxServiceTime = maxServiceTime;
	}

}
