package model;

public class Model {
	private int minIntArriving;
	private int maxIntArriving;
	private int minServiceTime;
	private int maxServiceTime;
	private int nrQueues; 
	private int simInt;
	private int nrClients;
	
	private String result;
	
	public int getMinIntArriving() {
		return minIntArriving;
	}
	public void setMinIntArriving(String minIntArrivin) {
		this.minIntArriving = Integer.parseInt(minIntArrivin);
	}
	public int getMaxIntArriving() {
		return maxIntArriving;
	}
	public void setMaxIntArriving(String maxIntArriving) {
		this.maxIntArriving = Integer.parseInt(maxIntArriving);
	}
	public int getMinServiceTime() {
		return minServiceTime;
	}
	public void setMinServiceTime(String minServiceTime) {
		this.minServiceTime = Integer.parseInt(minServiceTime);
	}
	public int getMaxServiceTime() {
		return maxServiceTime;
	}
	public void setMaxServiceTime(String maxServiceTime) {
		this.maxServiceTime = Integer.parseInt(maxServiceTime);
	}
	public int getNrQueues() {
		return nrQueues;
	}
	public void setNrQueues(String nrQueues) {
		this.nrQueues = Integer.parseInt(nrQueues);
	}
	public int getSimInt() {
		return simInt;
	}
	public void setSimInt(String simInt) {
		this.simInt = Integer.parseInt(simInt);
	}
	public int getNrClients() {
		return nrClients;
	}
	public void setNrClients(String nrClients) {
		this.nrClients = Integer.parseInt(nrClients);
	}
	
	public String getResult() {
		return this.result;
	}
	public void setResult(String result) {
		this.result= result;
	}
	public void startSim() {
		Simulation sim = new Simulation();
		Thread t = new Thread(sim);
		t.start();
	}
}
