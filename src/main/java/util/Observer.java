package util;

public interface Observer {

	public void notify(String notifyValue);
}
