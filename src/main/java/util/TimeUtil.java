package util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TimeUtil {

	private static SimpleDateFormat sdf = new SimpleDateFormat("mm:ss.SSS"); 
	
	public static Calendar initTime;
	public static Calendar simulationTime;
	
	public static String getFancyTime(Calendar c) {
		return sdf.format(c.getTime());
	}
}
