package util;

public interface Observable {

	public void addObserver(Observer ob);
}
