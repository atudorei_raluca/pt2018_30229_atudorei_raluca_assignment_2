package implementation;

import java.util.Calendar;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import logging.ClientLogger;
import util.TimeUtil;

public class QueueClient extends Thread {

	
	private BlockingQueue<Client> coadaClienti;
	private int nrCoada;
	private ClientLogger logger;
	private boolean sfarsitOrar;

	public QueueClient(int nrCoada, ClientLogger logger) {
		this.nrCoada = nrCoada;
		this.logger = logger;
		this.sfarsitOrar = false;
		coadaClienti = new LinkedBlockingQueue<Client>();
	}

	public void run() {
		try {
			while (!sfarsitOrar) {
				waitAddClient();
				Client c = coadaClienti.peek();
					Calendar crtTime = Calendar.getInstance();
					c.setServiceTimestamp(crtTime);
					logger.logServingClient(c, crtTime);
					// timpul la care s-a inceput servirea
					Thread.sleep(c.getServiceTime() * 1000);
					logger.logServingClientFinished(c, Calendar.getInstance());
				coadaClienti.poll();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Coada " + this.getNrCoada() + " a servit toti clientii la: "
				+ TimeUtil.getFancyTime(Calendar.getInstance()));
	}

	public synchronized void addClient(Client c) {
		
		c.setIdxCoada(this.getNrCoada());
		coadaClienti.add(c);
		notify();
	}
	public synchronized void waitAddClient() throws InterruptedException {
		while(coadaClienti.size()==0) {
			wait();
		}
	}

	public long size() {
		return coadaClienti.size();
	}

	public int getNrCoada() {
		return nrCoada;
	}

	public void setNrCoada(int nrCoada) {
		this.nrCoada = nrCoada;
	}

	public void setSfarsitOrar(boolean sfarsitOrar) {
		this.sfarsitOrar = sfarsitOrar;
	}

}
