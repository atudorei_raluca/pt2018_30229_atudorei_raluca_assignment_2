package implementation;

import java.util.Calendar;

public class Client {
	private int serviceTime;
	private long waitingTime;
	private Calendar arrivalTime, serviceTimestamp;
	private int idxCoada;
	private int id;

	public Client(int id, int serviceTime) {
		this.id = id;
		this.serviceTime = serviceTime;
		this.arrivalTime = Calendar.getInstance();
	}

	public int getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(int serviceTime) {
		this.serviceTime = serviceTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdxCoada() {
		return idxCoada;
	}

	public void setIdxCoada(int idxCoada) {
		this.idxCoada = idxCoada;
	}

	public Calendar getServiceTimestamp() {
		return serviceTimestamp;
	}

	public void setServiceTimestamp(Calendar serviceTimestamp) {
		this.serviceTimestamp = serviceTimestamp;
		this.waitingTime = (serviceTimestamp.getTimeInMillis() - arrivalTime.getTimeInMillis())/1000;
	}

	public long getWaitingTime() {
		return waitingTime;
	}

	
}
