package logging;
import java.util.Calendar;

import implementation.Client;

public interface ClientLogger {

	public void logNumberOfQueues(int noOfQueues);
	public void logAddClient(Client c, Calendar crtTime);
	public void logServingClient(Client c, Calendar crtTime);
	public void logServingClientFinished(Client c, Calendar crtTime);

}
