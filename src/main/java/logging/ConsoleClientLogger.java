package logging;

import java.util.Calendar;

import implementation.Client;
import util.TimeUtil;

public class ConsoleClientLogger implements ClientLogger {
	
	public synchronized void logAddClient(Client c, Calendar crtTime) {
		System.out.println("Clientul " + c.getId() + " a fost adaugat la coada " + c.getIdxCoada() + " la momentul "
				+ TimeUtil.getFancyTime(crtTime));
	}

	public synchronized void logServingClient(Client c, Calendar crtTime) {
		System.out.println("Servim clientul " + c.getId() + " timp de: " + c.getServiceTime() + " secunde in coada: "
				+ c.getIdxCoada() + " la timpul: " + TimeUtil.getFancyTime(crtTime) + " si a asteptat "
				+ c.getWaitingTime() + " secunde");

	}

	public synchronized void logServingClientFinished(Client c, Calendar crtTime) {
		System.out.println("Servirea clientului " + c.getId() + " la coada " + c.getIdxCoada()
				+ " s-a sfarsit la timpul: " + TimeUtil.getFancyTime(crtTime));

	}

	public synchronized void logNumberOfQueues(int noOfQueues) {
		System.out.println("S-au creat " + noOfQueues + " cozi");
	}

}
