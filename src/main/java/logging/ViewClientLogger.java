package logging;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import implementation.Client;
import util.Observable;
import util.Observer;

public class ViewClientLogger implements ClientLogger, Observable {

	private List<Observer> observers = new ArrayList<Observer>();
	private String clientStatus = "";
	private int nrCozi;

	public synchronized void logAddClient(Client c, Calendar crtTime) {
		String[] cozile = clientStatus.split("\n");
		String[] coadaTokens = cozile[c.getIdxCoada() - 1].split("=");
		String clienti;
		if (coadaTokens.length > 1) {
			clienti = coadaTokens[1];
		} else {
			clienti = "";
		}
		clienti += c.getId() + " ";
		rebuildStatus(cozile, clienti, c.getIdxCoada() - 1);

	}

	private synchronized void rebuildStatus(String[] cozile, String clienti, int nrCoada) {
		clientStatus = "";
		for (int i = 0; i < nrCozi; i++) {
			if (i != nrCoada) {
				clientStatus += cozile[i] + "\n";
			} else {
				clientStatus += "c" + (i + 1) + "=" + clienti + "\n";
			}
		}
		notifyObservers();
	}

	public synchronized void logServingClient(Client c, Calendar crtTime) {
		// do nothing by intention

	}

	public synchronized void logServingClientFinished(Client c, Calendar crtTime) {
		String[] cozile = clientStatus.split("\n");
		String[] coadaTokens = cozile[c.getIdxCoada() - 1].split("=");
		String clienti = coadaTokens[1].substring(coadaTokens[1].indexOf(" ") + 1, coadaTokens[1].length());
		rebuildStatus(cozile, clienti, c.getIdxCoada() - 1);
	}

	public synchronized void addObserver(Observer ob) {
		this.observers.add(ob);
	}

	private synchronized void notifyObservers() {
		for (Observer ob : observers) {
			ob.notify(clientStatus);
		}
	}

	public synchronized void logNumberOfQueues(int noOfQueues) {
		this.nrCozi = noOfQueues;
		for (int i = 1; i <= noOfQueues; i++) {
			this.clientStatus += "c" + i + "=\n";
		}
	}

}
