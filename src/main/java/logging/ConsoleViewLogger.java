package logging;

import java.util.Calendar;

import implementation.Client;

public class ConsoleViewLogger implements ClientLogger {

	private ConsoleClientLogger consoleLogger;
	private ViewClientLogger viewLogger;
	public ConsoleViewLogger(ConsoleClientLogger consolelogger, ViewClientLogger viewLogger ) {
		this.consoleLogger = consolelogger;
		this.viewLogger= viewLogger;
	}
	
	public synchronized void logNumberOfQueues(int noOfQueues) {
		viewLogger.logNumberOfQueues(noOfQueues);
		consoleLogger.logNumberOfQueues(noOfQueues);
	}

	public synchronized void logAddClient(Client c, Calendar crtTime) {
		viewLogger.logAddClient(c, crtTime);
		consoleLogger.logAddClient(c, crtTime);
	}

	public synchronized void logServingClient(Client c, Calendar crtTime) {
		viewLogger.logServingClient(c, crtTime);
		consoleLogger.logServingClient(c, crtTime);
	}

	public synchronized void logServingClientFinished(Client c, Calendar crtTime) {
		viewLogger.logServingClientFinished(c, crtTime);
		consoleLogger.logServingClientFinished(c, crtTime);
	}

}
