package mvc;

import implementation.Producer;
import logging.ClientLogger;
import logging.ConsoleClientLogger;

public class Model {
	private int minTimeBetween;
	private int maxTimeBetween;
	private int minServiceTime;
	private int maxServiceTime;
	private int nrQueues; 
	private int simInt;
	private String result;
	private ClientLogger logger;
	
	public int getMinTimeBetween() {
		return minTimeBetween;
	}
	public void setMinTimeBetween(String minTimeBetween) {
		this.minTimeBetween = Integer.parseInt(minTimeBetween);
	}
	public int getMaxTimeBetween() {
		return maxTimeBetween;
	}
	public void setMaxTimeBetween(String maxTimeBetween) {
		this.maxTimeBetween = Integer.parseInt(maxTimeBetween);
	}
	public int getMinServiceTime() {
		return minServiceTime;
	}
	public void setMinServiceTime(String minServiceTime) {
		this.minServiceTime = Integer.parseInt(minServiceTime);
	}
	public int getMaxServiceTime() {
		return maxServiceTime;
	}
	public void setMaxServiceTime(String maxServiceTime) {
		this.maxServiceTime = Integer.parseInt(maxServiceTime);
	}
	public int getNrQueues() {
		return nrQueues;
	}
	public void setNrQueues(String nrQueues) {
		this.nrQueues = Integer.parseInt(nrQueues);
	}
	public int getSimInt() {
		return simInt;
	}
	public void setSimInt(String simInt) {
		this.simInt = Integer.parseInt(simInt);
	}
	public String getResult() {
		return this.result;
	}
	public void setResult(String result) {
		this.result= result;
	}
	public ClientLogger getLogger() {
		return logger;
	}
	public void setLogger(ClientLogger logger) {
		this.logger = logger;
	}
	
	public void startSim() {
		Producer p= new Producer(nrQueues, simInt, minTimeBetween, maxTimeBetween, minServiceTime, maxServiceTime, logger);
		Thread t= new Thread(p);
		t.setPriority(10);
		t.start();
	}
}
