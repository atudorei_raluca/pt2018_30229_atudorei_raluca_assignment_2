package mvc;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import util.Observer;

public class View extends JFrame implements Observer {
	
	private JTextField minTimeBetween = new JTextField();
	private JTextField maxTimeBetween = new JTextField();
	private JTextField minServiceTime = new JTextField();
	private JTextField maxServiceTime = new JTextField();
	private JTextField nrQueues = new JTextField();
	private JTextField simInt = new JTextField();
	private JTextArea output = new JTextArea();
	private JButton start = new JButton("Start");
	private JScrollPane scroll= new JScrollPane(output);
	
	public View() {
		initUI();
	}

	public void initUI() {

		JFrame frame = new JFrame("Tema 2");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(900, 300);
		JPanel panelInput = new JPanel();
		panelInput.setSize(700,100);
		GridLayout layout = new GridLayout(0,5);
		layout.setHgap(10);
	    layout.setVgap(10);
	    panelInput.setLayout(layout);
		
		output.setPreferredSize(new Dimension(0, 300));
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		panelInput.add(new JLabel("Number of queues: "));
		panelInput.add(nrQueues);
		panelInput.add(new JLabel("Interval between customers: "));
		panelInput.add(minTimeBetween);
		panelInput.add(maxTimeBetween);
	
		panelInput.add(new JLabel("Simulation interval: "));
		panelInput.add(simInt);
		panelInput.add(new JLabel("Service time: "));
		panelInput.add(minServiceTime);
		panelInput.add(maxServiceTime);
		
		JPanel p = new JPanel();
		p.add(panelInput);
		p.add(start);
		p.add(scroll, BorderLayout.CENTER);

		p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
		frame.setContentPane(p);
		frame.setVisible(true);

		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public void addStartListener(ActionListener s) {
		start.addActionListener(s);
	}
	
	public String getMinTimeBetween() {
		return minTimeBetween.getText();
	}

	public String getMaxTimeBetween() {
		return maxTimeBetween.getText();
	}
	
	public String getMinServiceTime() {
		return minServiceTime.getText();
	}
	
	public String getMaxServiceTime() {
		return maxServiceTime.getText();
	}
	
	public String getNrQueues() {
		return nrQueues.getText();
	}
	
	public String getSimInt() {
		return simInt.getText();
	}
	
	public void setResult(String o) {
		output.append(o);
	}

	public void notify(String notifyValue) {
		output.setText(notifyValue);
	}



}
