package mvc;

import logging.ClientLogger;
import logging.ConsoleClientLogger;
import logging.ConsoleViewLogger;
import logging.ViewClientLogger;
import util.Observable;
import util.Observer;

public class MVC {
	public static void main (String[] args) {

        Model      model      = new Model();
        View       view       = new View();
        ViewClientLogger vcLogger = new ViewClientLogger();
        Controller controller = new Controller(view, model, (Observable) vcLogger);
        
        ClientLogger cLogger = new ConsoleViewLogger(new ConsoleClientLogger(), vcLogger);
        model.setLogger(cLogger);
        
        view.setVisible(true);
     }
}
