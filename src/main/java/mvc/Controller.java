package mvc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import util.Observable;
import util.Observer;

public class Controller {
	private View view;
	private Model model;
	public Controller(View view, Model model, Observable observable) {
		this.view= view;
		this.model= model;
		observable.addObserver((Observer)view);
		view.addStartListener(new StartListener());
	}
	
	class StartListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String minTimeBetween= view.getMinTimeBetween();
			model.setMinTimeBetween(minTimeBetween);
			String maxTimeBetween= view.getMaxTimeBetween();
			model.setMaxTimeBetween(maxTimeBetween);
			String minServiceTime= view.getMinServiceTime();
			model.setMinServiceTime(minServiceTime);
			String maxServiceTime= view.getMaxServiceTime();
			model.setMaxServiceTime(maxServiceTime);
			String nrQueues= view.getNrQueues();
			model.setNrQueues(nrQueues);
			String simInt= view.getSimInt();
			model.setSimInt(simInt);
			model.startSim();
			String result= model.getResult();
			view.setResult(result);
			
		}
	}
}
